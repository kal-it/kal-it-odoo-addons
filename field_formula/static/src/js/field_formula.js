/* Copyright 2021 Kal-It (https://kal-it.fr)
 * License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
 */

odoo.define("field_formula.Formula", function (require) {
    "use strict";

    var NumericField = require("web.basic_fields").NumericField;
    var field_utils = require('web.field_utils');

    NumericField.include({
        _parseValue: function (value) {
            if (this.mode === 'edit' && value.startsWith("=")) {
                // Replace any character that is not related to a formula (eg. letters) and the equal sign
                var value = value.replace(/[^-()\d/*+.,]/g, '');
                // string => array and filter out empty strings
                var value_array = value.split(/([-()/*+])/).filter(e => e);
                // Ensure respect of user language
                value_array.forEach(function (element, index) {
                    try {
                        var parsed_float = field_utils.parse["float"](element);
                        this[index] = parsed_float;

                    } catch { };
                }, value_array);
                // Evaluate Expression
                value = eval(value_array.join(""));
                this.$input.val(value);
            }
            return this._super.apply(this, arguments);
        },
    });
});
