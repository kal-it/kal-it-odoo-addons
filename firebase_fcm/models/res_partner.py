# Copyright 2021 Kal-It (https://kal-it.fr)
# @author Timothée Ringeard <timothee.ringeard@kal-it.fr>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

from odoo import fields, models


class ResPartner(models.Model):
    _inherit = "res.partner"

    fcm_tokens = fields.One2many(
        comodel_name="firebase.fcm.device.token",
        inverse_name="partner_id",
        string="Firebase - FCM Tokens",
        copy=False,
    )

    def send_fcm_notifications(self, message_title, message_body):
        registration_ids = self.mapped("fcm_tokens.token")
        self.env["firebase.fcm"].send_notifications(
            registration_ids, message_title, message_body
        )
