# Copyright 2021 Kal-It (https://kal-it.fr)
# @author Timothée Ringeard <timothee.ringeard@kal-it.fr>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

from odoo import fields, models


class FCMDeviceToken(models.Model):
    _name = "firebase.fcm.device.token"
    _description = "Firebase - FCM Device Token"

    token = fields.Char("Token",)
    partner_id = fields.Many2one(comodel_name="res.partner", string="Partner",)
