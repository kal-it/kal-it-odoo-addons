# Copyright 2020 Kal-It (https://kal-it.fr)
# @author Timothée Ringeard <timothee.ringeard@kal-it.fr>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

import os

from odoo import api, models

from .apm_client import elasticapm, is_true

BaseModel = models.BaseModel
odoo_search = BaseModel.search
odoo_unlink = BaseModel.unlink
odoo_write = BaseModel.write
odoo_create = BaseModel.create


def span_parameters(self, method):
    return {
        "name": "ORM - %s - %s" % (self._name, method),
        "span_type": "%s.%s.%s" % ("orm", self._name, method),
    }


@api.model
def search(self, *args, **kwargs):
    with elasticapm.capture_span(**span_parameters(self, "search")):
        return odoo_search(self, *args, **kwargs)


@api.multi
def unlink(self):
    with elasticapm.capture_span(**span_parameters(self, "unlink")):
        return odoo_unlink(self)


@api.multi
def write(self, vals):
    with elasticapm.capture_span(**span_parameters(self, "write")):
        return odoo_write(self, vals)


@api.model_create_multi
@api.returns('self', lambda value: value.id)
def create(self, vals):
    with elasticapm.capture_span(**span_parameters(self, "create")):
        return odoo_create(self, vals)


if is_true(os.environ.get("ELASTIC_APM")):
    BaseModel.search = search
    BaseModel.unlink = unlink
    BaseModel.write = write
    BaseModel.create = create
